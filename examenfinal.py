import math

class examen:
    def calcular_movimiento(x,y,z):

        lista2=[]

        m2=0
        aceleracion=0
        x=math.radians(x)

        while m2<1000000000 and aceleracion>=0:
    
            lista=[m2]
        
            aceleracion=10*(z*math.sin(x)-m2-y*z*math.cos(x))/(m2+z)
    
            if(aceleracion>0):
                direccion= "subiendo"
            
            else:
                direccion= "bajando"

            lista.append(direccion)
            lista2.append(lista)
            m2=m2+0.5
           
        return lista2

    def calcular_angulo(a,b,c):


        M=b/a

        if(b>a):
            if(c<math.sqrt((M*M)-1)):
                return -1
            else:
                angulo1=(M+c*math.sqrt(1-(M*M)+(c*c)))/(1+(c*c))
                angulo1=math.asin(angulo1)
                angulo1=math.degrees(angulo1)

                angulo2=(M-c*math.sqrt(1-(M*M)+(c*c)))/(1+(c*c))
                angulo2=math.asin(angulo2)
                angulo2=math.degrees(angulo2)

                if (angulo1>=10 and angulo1<=85):
                    resultado1=angulo1
        
            
                if(angulo2>=10 and angulo2<=85):
                    resultado2=angulo2
        


                return(resultado1, resultado2)
        else:
            angulo1=(M+c*math.sqrt(1-(M*M)+(c*c)))/(1+(c*c))
            angulo1=math.asin(angulo1)
            angulo1=math.degrees(angulo1)

            angulo2=(M-c*math.sqrt(1-(M*M)+(c*c)))/(1+(c*c))
            angulo2=math.asin(angulo2)
            angulo2=math.degrees(angulo2)

            if (angulo1>=10 and angulo1<=85):
                resultado1=angulo1
        
            
            if(angulo2>=10 and angulo2<=85):
                resultado2=angulo2
        


            return(resultado1, resultado2)




print(examen.calcular_movimiento(55,0.15,6))
print("Los valores del ángulo son: ", examen.calcular_angulo(1,0.9,0.4))     

    

